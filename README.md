Role Name
=========

A role that installs the Ariadne Portal docker stack

Role Variables
--------------

The most important variables are listed below:

``` yaml
ariadne_portal_compose_dir: '/srv/ariadne_portal_stack'
ariadne_portal_docker_stack_name: 'ariadne-portal'
ariadne_portal_docker_service_client_name: 'client'
ariadne_portal_docker_client_image: 'ariadneplusportal/www-portal-client'
# IMPORTANT. Set it to True for the server that is going to host the DB
ariadne_portal_behind_haproxy: True
ariadne_portal_haproxy_public_net: 'haproxy-public'
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
